<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?= $title_page ?></title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo TEMPLATE_PATH ?>kioshotel/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo TEMPLATE_PATH ?>kioshotel/css/bootstrap.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <!-- <link href="<?php echo TEMPLATE_PATH ?>kioshotel/css/mdb.min.css" rel="stylesheet"> -->
    <!-- Your custom styles (optional) -->
    <link href="<?php echo TEMPLATE_PATH ?>kioshotel/css/style.css" rel="stylesheet">
</head>

<body>
<div class="container">
    <!--Navigation & Intro-->
    <header>
        <nav class="navbar fixed-top navbar-toggleable-md navbar-dark scrolling-navbar">
            <div class="container">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <img src="<?php echo base_url() ?>uploads/logo.png" height="43" class="d-inline-block align-top" alt="KIOSHOTEL">
                </a>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav mr-auto">
                    </ul>
                    <ul class="navbar-nav nav-flex-icons">
                        <li class="nav-item">
                            <a class="nav-link"><b>New Realese</b></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"><b>|</b></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"><b>Promo</b></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"><b>|</b></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"><b>Kuis</b></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <br><br>
    <div class="view row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Hotel</a></li>
                <li class="breadcrumb-item"><a href="#">Budget</a></li>
                <li class="breadcrumb-item active">Villas</li>
                <li class="breadcrumb-item active">Where To Go</li>
            </ol>
        </div>
        <div class="col-md-3 leftside">
            <h3>HOTELS</h3>
            <div class="card" style="margin-bottom: 5px;">
                <div class="card-body">
                    <h4>Bintang Hotel</h4>
                    <div class="checkbox">
                        <input type="checkbox" name="star5" id="star5"> 
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </div>
                    <div class="checkbox">
                        <input type="checkbox" name="star4" id="star4"> 
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </div>
                    <div class="checkbox">
                        <input type="checkbox" name="star3" id="star3"> 
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </div>
                    <div class="checkbox">
                        <input type="checkbox" name="star2" id="star2"> 
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </div>
                </div>
            </div>
            <!-- Area Hotel -->
            <div class="card" style="margin-bottom: 5px;">
                <div class="card-body">
                    <h4>Hotel Area :</h4>
                    <div class="form-group">
                        <select name="lokasi" class="form-control">
                            <option value="">[ Pilih Area ]</option>
                        </select>
                    </div>
                    <div class="listArea">
                        <div class="checkbox">
                            <input type="checkbox" name="star2" id="star2"> Bandung Barat <span class="pull-right">(121)</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Tipe Hotel -->
            <div class="card" style="margin-bottom: 5px;">
                <div class="card-body">
                    <h4>tipe Hotel :</h4>
                    <div class="tipeHotel">
                        <div class="checkbox">
                            <input type="checkbox" name="star2" id="star2"> Hotel
                        </div>
                        <div class="checkbox">
                            <input type="checkbox" name="star2" id="star2"> Villa
                        </div>
                        <div class="checkbox">
                            <input type="checkbox" name="star2" id="star2"> Guest House
                        </div>
                    </div>
                </div>
            </div>
            <!-- Maps Hotel -->
            <div class="card" style="margin-bottom: 5px;">
                <div class="card-body"><h4>Lokasi</h4></div>
                <div class="card-bodyx">
                    <div class="googlemaps">
                        <iframe width="100%" height="100%" frameborder="0" style="border:0"
src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJuaGIU3_oaC4RD_HY7WSLAhE&key=AIzaSyD9tvXeU4FNdGD0YQMDy3lxr-pG6s8-RzM" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-9">
            <ul class="list-group">
                <?php for($i=1; $i<5; $i++){ ?>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="http://success-at-work.com/wp-content/uploads/2015/04/free-stock-photos.gif" class="thumbnail" width="100%">
                        </div>
                        <div class="col-md-8">
                            <h5>Excellent Seven Boutique Hotel</h5>
                            <div class="star"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
                            <div class="address">
                                Jl.Pasir Kaliki (Paskal Hyper Square Blok D No.29-32), Bandung
                            </div>
                            <div class="viewlocation">
                                <a href="#"><i class="fa fa-map-marker"></i> Lihat lokasi</a>
                            </div>

                            <div class="fasilitas row">
                                <div class="col-md-8">
                                    <div class="icon-wifi"></div>
                                    <div class="icon-towel"></div>
                                    <div class="icon-television"></div>
                                    <div class="icon-slippers"></div>
                                    <div class="icon-sleeping-bed-silhouette"></div>
                                    <div class="icon-shower"></div>
                                    <div class="icon-restaurant"></div>
                                    <div class="icon-parking"></div>
                                    <div class="icon-minisplit"></div>
                                    <div class="icon-menu"></div>
                                    <div class="icon-meal"></div>
                                    <div class="icon-location"></div>
                                    <div class="icon-information"></div>
                                    <div class="icon-hairdryer"></div>
                                    <div class="icon-beds"></div>
                                    <div class="icon-bed"></div>
                                    <div class="icon-bathtub"></div>
                                    <div class="icon-bathrobe"></div>
                                </div>
                                <div class="col-md-4">
                                    <div class="text-center">
                                        Harga<br>
                                        <span class="alert-info" style="padding: 5px;">Rp. 300.000,-</span>
                                    </div>
                                </div>
                            </div>
                            <dir class="circle text-alert">50%</dir>
                        </div>
                    </div>
                </li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <br><br>
    <!-- <div class="row"> -->
    <nav class="nav nav-tabs" id="myTab" role="tablist">
      <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-kota" role="tab" aria-controls="nav-home" aria-selected="true">Bandung Kota</a>
      <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-barat" role="tab" aria-controls="nav-profile" aria-selected="false">Bandung Barat</a>
      <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-timur" role="tab" aria-controls="nav-contact" aria-selected="false">Bandung Timur</a>
      <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-selatan" role="tab" aria-controls="nav-contact" aria-selected="false">Bandung Selatan</a>
    </nav>
    <div class="tab-content" id="nav-tabContent" style="padding:10px 20px;">
      <div class="tab-pane fade show active" id="nav-kota" role="tabpanel" aria-labelledby="nav-home-tab" style="padding:0px; margin:0px;">
        <h2>new release</h2>
           <?php $i = 1; while ( $i <= 4) { ?>
           <!-- <div class="row row-margin-bottom"> -->
            <div class="col-md-6 lib-item pull-left" data-category="view">
                <div class="lib-panel">
                    <div class="row box-shadow">
                        <div class="col-md-6">
                            <img class="lib-img-show" src="http://lorempixel.com/850/850/?random=123">
                        </div>
                        <div class="col-md-6">
                            <div class="lib-row lib-header">
                                Example library
                                <div class="lib-header-seperator"></div>
                            </div>
                            <div class="lib-row lib-desc">
                                Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $i++; } ?>
            <div class="clearfix"></div>
      </div>
      <div class="tab-pane fade" id="nav-barat" role="tabpanel" aria-labelledby="nav-profile-tab">
        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
      </div>
      <div class="tab-pane fade" id="nav-timur" role="tabpanel" aria-labelledby="nav-contact-tab">
        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
      </div>
      <div class="tab-pane fade" id="nav-selatan" role="tabpanel" aria-labelledby="nav-contact-tab">
        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
      </div>
    </div>
    <hr/>
    <div class="row promo">
        <div class="col-md-12"><h2 style="border-bottom: 3px solid #AAF200; display: inline-block;">Promo</h2></div>
        <!-- <div class="clearfix"></div><br><br> -->
        <div class="col-md-4">
            <img class="img-responsive" style="width: 100%;" src="<?php echo TEMPLATE_PATH ?>kioshotel/img/promo-thumb.png" />
        </div>
        <div class="col-md-4">
            <img class="img-responsive" style="width: 100%;" src="<?php echo TEMPLATE_PATH ?>kioshotel/img/promo-thumb.png" />
        </div>
        <div class="col-md-4">
            <img class="img-responsive" style="width: 100%;" src="<?php echo TEMPLATE_PATH ?>kioshotel/img/promo-thumb.png" />
        </div>
    </div>
    <!-- </div> -->
    <br><br>
    <!-- quiz -->
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 50px;">
            <h2>quiz hari ini</h2>
            <img src="<?php echo TEMPLATE_PATH ?>kioshotel/img/quiz.jpg" class="thumbnail" width="100%">
        </div>
        <div class="col-md-12" align="center" style="margin-bottom: 50px;">
            <h3>Hotels Partner</h3>
            
        </div>
    </div>

    <br>
</div>
    <div id="footer" class="container-fluid">
        <div class="row">
            <div class="col-md-12" style="margin-top: 50px; color: #fff">
                <div class="row" align="center">
                    <div class="col-md-4">
                        asdasdas
                    </div>
                    <div class="col-md-4">
                        asdasdas
                    </div>
                    <div class="col-md-4">
                        asdasdas
                    </div>
                </div>
            </div>
        </div>
    </div>
  <!--/.Navigation & Intro-->
  <!--Main layout-->
<!--   <main class="pt-6x text-center">
      <h2>Your content</h2>
      zxaAS
  </main> -->
  <!--/Main layout-->

    <!-- /Start your project here-->

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="<?php echo TEMPLATE_PATH ?>kioshotel/js/jquery-3.1.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo TEMPLATE_PATH ?>kioshotel/js/tether.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo TEMPLATE_PATH ?>kioshotel/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo TEMPLATE_PATH ?>kioshotel/js/mdb.min.js"></script>
    <script type="text/javascript" src="<?php echo TEMPLATE_PATH ?>kioshotel/js/typeahead.bundle.js"></script>
    <script type="text/javascript" src="<?php echo TEMPLATE_PATH ?>kioshotel/js/app.js"></script>
    <script type="text/javascript">
        
    </script>
</body>

</html>
