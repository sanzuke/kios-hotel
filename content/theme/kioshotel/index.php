<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Material Design Bootstrap</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo TEMPLATE_PATH ?>kioshotel/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo TEMPLATE_PATH ?>kioshotel/css/bootstrap.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <!-- <link href="<?php echo TEMPLATE_PATH ?>kioshotel/css/mdb.min.css" rel="stylesheet"> -->
    <!-- Your custom styles (optional) -->
    <link href="<?php echo TEMPLATE_PATH ?>kioshotel/css/style.css" rel="stylesheet">
</head>

<body>
<div class="container">
    <!-- Start your project here-->
    <!-- <div style="height: 100vh">
        <div class="flex-center flex-column">
            <h1 class="animated fadeIn mb-2">Material Design for Bootstrap</h1>

            <h5 class="animated fadeIn mb-1">Thank you for using our product. We're glad you're with us.</h5>

            <p class="animated fadeIn text-muted">MDB Team</p>
        </div>
    </div> -->

    <!--Navigation & Intro-->
    <header>
        <nav class="navbar fixed-top navbar-toggleable-md navbar-dark scrolling-navbar">
            <div class="container">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <img src="<?php echo base_url() ?>uploads/logo.png" height="43" class="d-inline-block align-top" alt="KIOSHOTEL">
                </a>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav mr-auto">
                        <!-- <li class="nav-item active">
                            <a class="nav-link">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link">Features</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link">Pricing</a>
                        </li>
                        <li class="nav-item btn-group">
                            <a class="nav-link dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <a class="dropdown-item">Action</a>
                                <a class="dropdown-item">Another action</a>
                                <a class="dropdown-item">Something else here</a>
                            </div>
                        </li> -->
                    </ul>
                    <ul class="navbar-nav nav-flex-icons">
                        <!-- <li class="nav-item">
                            <a class="nav-link"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"><i class="fa fa-instagram"></i></a>
                        </li> -->
                        <li class="nav-item">
                            <a class="nav-link"><b>New Realese</b></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"><b>|</b></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"><b>Promo</b></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"><b>|</b></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"><b>Kuis</b></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!--Mask-->
        <div class="view row">
            <!-- <div class="full-bg-img flex-center">
                <div class="container">
                    <div class="white-text text-center wow fadeInUp">
                        <h2>This Navbar is fixed</h2>
                        <h5>It will always stay visible on the top, even when you scroll down</h5>
                        <br>
                        <p>Full page intro with background image will be always displayed in full screen mode, regardless of device </p>
                    </div>
                </div>
            </div> -->
            <!-- <img src="<?php echo base_url() ?>uploads/banner.png"> -->
            <div class="col-md-12">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                  </ol>
                  <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                      <img class="d-block img-fluid" src="<?php echo base_url() ?>uploads/banner.png" alt="First slide">
                    </div>
                    <div class="carousel-item">
                      <img class="d-block img-fluid" src="<?php echo base_url() ?>uploads/banner.png" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                      <img class="d-block img-fluid" src="<?php echo base_url() ?>uploads/banner.png" alt="Third slide">
                    </div>
                  </div>
                  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
            </div>
        </div>
        <!--/.Mask-->
        <br>
        <div class="row">
            <div class="col-md-1">&nbsp;</div>

            <div class="col-md-10">
                <div class="cardx border-book">
                    <form>
                      <div class="row">
                        <div class="col">
                          <input type="text" id="tujuan" class="form-control typeahead" placeholder="Tujuan">
                        </div>
                        <div class="col">
                          <input type="date" class="form-control" placeholder="Check In">
                        </div>
                        <div class="col">
                          <input type="text" class="form-control" placeholder="Malam">
                        </div>
                        <div class="col">
                          <input type="date" class="form-control" placeholder="Check Out">
                        </div>
                        <div class="col" align="center">
                          <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i>Cari</button>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </form>
                </div>
            </div>

            <div class="col-md-1">&nbsp;</div>
        </div>
    </header>
    <br><br>
    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-6">
                    <div class="kategori">
                      <img src="<?php echo TEMPLATE_PATH ?>kioshotel/img/budget.png" alt="Avatar" class="image" style="width:100%">
                      <div class="middle">
                        <div class="text">HOTEL<br>BUDGET</div>
                      </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="kategori">
                      <img src="<?php echo TEMPLATE_PATH ?>kioshotel/img/budget.png" alt="Avatar" class="image" style="width:100%">
                      <div class="middle">
                        <div class="text">HOTEL<br>BINTANG</div>
                      </div>
                    </div>
                </div>
                <div class="col-md-6" style="margin-top: 30px;">
                    <div class="kategori">
                      <img src="<?php echo TEMPLATE_PATH ?>kioshotel/img/budget.png" alt="Avatar" class="image" style="width:100%">
                      <div class="middle">
                        <div class="text">VILLAS</div>
                      </div>
                    </div>
                </div>
                <div class="col-md-6" style="margin-top: 30px;">
                    <div class="kategori">
                      <img src="<?php echo TEMPLATE_PATH ?>kioshotel/img/budget.png" alt="Avatar" class="image" style="width:100%">
                      <div class="middle">
                        <div class="text">WHERE TO GO!</div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
             <!-- <div class="panel panel-danger">
                <div class="panel-heading">Judul</div>
                <div class="panel-body">
                    asd asdas da
                </div>
             </div> -->
             <!-- <div class="card text-center" >
              <div class="card-header">
                Featured
              </div>
              <img class="card-img-top" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22318%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20318%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_1607a0644ab%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A16pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_1607a0644ab%22%3E%3Crect%20width%3D%22318%22%20height%3D%22180%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22117.515625%22%20y%3D%2297.2%22%3E318x180%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="Card image cap">
              <div class="card-body">
                <h4 class="card-title">Nama Hotel</h4>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <p><b>Rp. 200.000</b></p>
                <p>Harga Awal</p>
                <p>Rp. 350.000</p>
              </div>
              <div class="card-footer text-muted">
                <a href="#" class="btn btn-success pull-right"><i class="fa fa-search"></i> Lihat</a>
              </div>
            </div> -->

            <div class="card">
                    <div class="card-block">
                        <figure class="profile" style="height: 150px; overflow: hidden;">
                            <img src="http://success-at-work.com/wp-content/uploads/2015/04/free-stock-photos.gif" class="profile-avatar" width="100%" alt="">
                        </figure>
                        <div class="card-body">
                            <h4 class="card-title mt-3">Tawshif Ahsan Khan</h4>
                            <div class="meta">
                                <a>Friends</a>
                            </div>
                            <div class="card-text">
                                Tawshif is a web designer living in Bangladesh.
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <small>Last updated 3 mins ago</small>
                        <button class="btn btn-secondary float-right btn-sm">show</button>
                    </div>
                </div>
        </div>
    </div>
    <br><br>
    <!-- <div class="row"> -->
    <nav class="nav nav-tabs" id="myTab" role="tablist">
      <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-kota" role="tab" aria-controls="nav-home" aria-selected="true">Bandung Kota</a>
      <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-barat" role="tab" aria-controls="nav-profile" aria-selected="false">Bandung Barat</a>
      <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-timur" role="tab" aria-controls="nav-contact" aria-selected="false">Bandung Timur</a>
      <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-selatan" role="tab" aria-controls="nav-contact" aria-selected="false">Bandung Selatan</a>
    </nav>
    <div class="tab-content" id="nav-tabContent" style="padding:10px 20px;">
      <div class="tab-pane fade show active" id="nav-kota" role="tabpanel" aria-labelledby="nav-home-tab" style="padding:0px; margin:0px;">
        <h2>new release</h2>
           <?php $i = 1; while ( $i <= 4) { ?>
           <!-- <div class="row row-margin-bottom"> -->
            <div class="col-md-6 lib-item pull-left" data-category="view">
                <div class="lib-panel">
                    <div class="row box-shadow">
                        <div class="col-md-6">
                            <img class="lib-img-show" src="http://lorempixel.com/850/850/?random=123">
                        </div>
                        <div class="col-md-6">
                            <div class="lib-row lib-header">
                                Example library
                                <div class="lib-header-seperator"></div>
                            </div>
                            <div class="lib-row lib-desc">
                                Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $i++; } ?>
            <div class="clearfix"></div>
      </div>
      <div class="tab-pane fade" id="nav-barat" role="tabpanel" aria-labelledby="nav-profile-tab">
        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
      </div>
      <div class="tab-pane fade" id="nav-timur" role="tabpanel" aria-labelledby="nav-contact-tab">
        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
      </div>
      <div class="tab-pane fade" id="nav-selatan" role="tabpanel" aria-labelledby="nav-contact-tab">
        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
      </div>
    </div>
    <hr/>
    <div class="row promo">
        <div class="col-md-12"><h2 style="border-bottom: 3px solid #AAF200; display: inline-block;">Promo</h2></div>
        <!-- <div class="clearfix"></div><br><br> -->
        <div class="col-md-4">
            <img class="img-responsive" style="width: 100%;" src="<?php echo TEMPLATE_PATH ?>kioshotel/img/promo-thumb.png" />
        </div>
        <div class="col-md-4">
            <img class="img-responsive" style="width: 100%;" src="<?php echo TEMPLATE_PATH ?>kioshotel/img/promo-thumb.png" />
        </div>
        <div class="col-md-4">
            <img class="img-responsive" style="width: 100%;" src="<?php echo TEMPLATE_PATH ?>kioshotel/img/promo-thumb.png" />
        </div>
    </div>
    <!-- </div> -->
    <br><br>
    <!-- quiz -->
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 50px;">
            <h2>quiz hari ini</h2>
            <img src="<?php echo TEMPLATE_PATH ?>kioshotel/img/quiz.jpg" class="thumbnail" width="100%">
        </div>
        <div class="col-md-12" align="center" style="margin-bottom: 50px;">
            <h3>Hotels Partner</h3>
            
        </div>
    </div>

    <br>
</div>
    <div id="footer" class="container-fluid">
        <div class="row">
            <div class="col-md-12" style="margin-top: 50px; color: #fff">
                <div class="row" align="center">
                    <div class="col-md-4">
                        asdasdas
                    </div>
                    <div class="col-md-4">
                        asdasdas
                    </div>
                    <div class="col-md-4">
                        asdasdas
                    </div>
                </div>
            </div>
        </div>
    </div>
  <!--/.Navigation & Intro-->
  <!--Main layout-->
<!--   <main class="pt-6x text-center">
      <h2>Your content</h2>
      zxaAS
  </main> -->
  <!--/Main layout-->

    <!-- /Start your project here-->

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="<?php echo TEMPLATE_PATH ?>kioshotel/js/jquery-3.1.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo TEMPLATE_PATH ?>kioshotel/js/tether.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo TEMPLATE_PATH ?>kioshotel/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo TEMPLATE_PATH ?>kioshotel/js/mdb.min.js"></script>
    <script type="text/javascript" src="<?php echo TEMPLATE_PATH ?>kioshotel/js/typeahead.bundle.js"></script>
    <script type="text/javascript" src="<?php echo TEMPLATE_PATH ?>kioshotel/js/app.js"></script>
    <script type="text/javascript">
        
    </script>
</body>

</html>
