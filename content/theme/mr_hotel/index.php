<!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title><?php echo $sitename ?> - <?php echo $page_title ?></title>
<link href="<?php echo TEMPLATE_PATH ?>mr_hotel/css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php echo TEMPLATE_PATH ?>mr_hotel/js/jquery-1.11.0.min.js"></script>
<!-- Custom Theme files -->
<link href="<?php echo TEMPLATE_PATH ?>mr_hotel/css/style.css" rel="stylesheet" type="text/css" media="all"/>
<link href="<?php echo TEMPLATE_PATH ?>mr_hotel/css/style1.css" rel="stylesheet" type="text/css" media="all" />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Mr Hotel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--Google Fonts-->
<link href='//fonts.googleapis.com/css?family=Hind:400,300' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Aladin' rel='stylesheet' type='text/css'>
<!--google fonts-->
<!-- animated-css -->
		<link href="<?php echo TEMPLATE_PATH ?>mr_hotel/css/animate.css" rel="stylesheet" type="text/css" media="all">
		<script src="<?php echo TEMPLATE_PATH ?>mr_hotel/js/wow.min.js"></script>
		<script>
		 new WOW().init();
		</script>
<!-- animated-css -->
<script src="<?php echo TEMPLATE_PATH ?>mr_hotel/js/modernizr.js"></script>
<script src="<?php echo TEMPLATE_PATH ?>mr_hotel/js/app.js"></script>
<script src="<?php echo TEMPLATE_PATH ?>mr_hotel/js/bootstrap.min.js"></script>
<script src="<?php echo TEMPLATE_PATH ?>mr_hotel/js/bootstrap-datepicker.js"></script>
</head>
<body>
<!--header-top start here-->
<div class="top-header">
	<div class="container">
		<div class="top-header-main">
			<div class="col-md-4 top-social wow bounceInLeft" data-wow-delay="0.3s">
			    <ul>
			    	<li><h5>Follow Us :</h5></li>
			    	<li><a href="<?php echo $fb ?>" target="_blank"><span class="fb"> </span></a></li>
			    	<li><a href="<?php echo $tw ?>" target="_blank"><span class="tw"> </span></a></li>
			    	<!-- <li><a href="#"><span class="in"> </span></a></li> -->
			    	<li><a href="<?php echo $ig ?>" target="_blank"><span class="gmail"> </span></a></li>
			    </ul>
			</div>
			<div class="col-md-8 header-address wow bounceInRight" data-wow-delay="0.3s">
				<ul>
					<li><span class="phone"> </span> <h6><?php echo $ph ?></h6></li>
					<li><span class="email"> </span><h6><a href="<?php echo $em ?>"><?php echo $em ?></a></h6></li>
				</ul>
			</div>
		  <div class="clearfix"> </div>
		</div>
	</div>
</div>
<!--header-top end here-->
<!--header start here-->
	<!-- NAVBAR
		================================================== -->
<div class="header">
	<div class="fixed-header">

		    <div class="navbar-wrapper">
		      <div class="container">
		        <nav class="navbar navbar-inverse navbar-static-top">
		             <div class="navbar-header">
			              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			                <span class="sr-only">Toggle navigation</span>
			                <span class="icon-bar"></span>
			                <span class="icon-bar"></span>
			                <span class="icon-bar"></span>
			              </button>
			              <div class="logo wow slideInLeft" data-wow-delay="0.3s">
			                    <a class="navbar-brand" href="<?php echo base_url() ?>"><img src="<?php echo TEMPLATE_PATH ?>mr_hotel/images/<?php echo $logo ?>" /></a>
			              </div>
			          </div>
		            <div id="navbar" class="navbar-collapse collapse">
		            <nav class="cl-effect-16" id="cl-effect-16">
		              <ul class="nav navbar-nav">
										<?php
											$h = ''; $p =''; $l =''; $k = ''; $g =''; $k =''; $uri = $this->uri->segment(1);
											switch ($uri) {
												case 'profil': $h = ''; $p ='active'; $l =''; $km = ''; $g =''; $k =''; break;
												case 'layanan': $h = ''; $p =''; $l ='active'; $km = ''; $g =''; $k =''; break;
												case 'kamar': $h = ''; $p =''; $l =''; $km = 'active'; $g =''; $k =''; break;
												case 'galeri': $h = ''; $p =''; $l =''; $km = ''; $g ='active'; $k =''; break;
												case 'kontak': $h = ''; $p =''; $l =''; $km = ''; $g =''; $k ='active'; break;
												default: $h = 'active'; $p =''; $l =''; $km = ''; $g =''; $k =''; break;
											}
										?>
		               	<li><a class="<?php echo $h ?>" href="<?php echo base_url() ?>" data-hover="Home">Home</a></li>
		                <li><a class="<?php echo $p ?>" href="<?php echo base_url() ?>profil" data-hover="Profil">Profil</a></li>
										<li><a class="<?php echo $l ?>" href="<?php echo base_url() ?>layanan" data-hover="Layanan">Layanan</a></li>
										<li><a class="<?php echo $km ?>" href="<?php echo base_url() ?>kamar" data-hover="Kamar">Kamar</a></li>
										<li><a class="<?php echo $g ?>" href="<?php echo base_url() ?>galeri" data-hover="Galeri">Galeri</a></li>
										<li><a class="<?php echo $k ?>" href="<?php echo base_url() ?>kontak" data-hover="Kontak">Kontak</a></li>
		              </ul>
		            </nav>

		            </div>
		            <div class="clearfix"> </div>
		             </nav>
		          </div>
		           <div class="clearfix"> </div>
		    </div>
	 </div>
</div>
<!--header end here-->
<?php if($this->uri->segment(1) == "") { ?>
	<?php echo $this->uri->segment(1) ?>
<!--banner  start hwew-->
<div class="banner">
	<div class="container">
		<div class="banner-main wow zoomIn" data-wow-delay="0.3s">
			<h2><?php echo $sitename ?></h2>
			<h6>Selamat datang di hotel kami</h6>
			<p>Hotel &amp; Balai Pertemuan</p>
		</div>
    </div>
</div>
<!--BANNER END HERE-->
<?php } ?>
<?php $this->load->view($page_view, true); ?>
<!--footer start here-->
<div class="footer">
	<div class="container">
		<div class="footer-main">
			<!-- <div class="col-md-3 ftr-grid wow zoomIn" data-wow-delay="0.3s">
				<div class="ftr-logo">
				<img src="<?php echo TEMPLATE_PATH ?>mr_hotel/images/ftr-logo.png"  alt="">
				</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
				<a href="single.html" class="ftr-btn">Read More</a>
			</div> -->
			<div class="col-md-4 ftr-grid ftr-mid wow zoomIn" data-wow-delay="0.3s">
				 <h3>Address</h3>
				 <span class="ftr-line flm"> </span>
				<?php echo $alamat ?>
			</div>
			<div class="col-md-4 ftr-grid ftr-rit wow zoomIn" data-wow-delay="0.3s">
				 <h3>Contact Us</h3>
				 <form>
				 	<input type="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}">
				 	<input type="submit" value="Send" />
				 </form>
				  <ul class="ftr-icons">
						<li><a href="<?php echo $fb ?>" target="_blank"><span class="fb"> </span></a></li>
			    	<li><a href="<?php echo $tw ?>" target="_blank"><span class="tw"> </span></a></li>
			    	<!-- <li><a href="#"><span class="in"> </span></a></li> -->
			    	<li><a href="<?php echo $ig ?>" target="_blank"><span class="gmail"> </span></a></li>
				 </ul>
			</div>
			<div class="col-md-4 ftr-grid ftr-last-gd ftr-rit wow zoomIn" data-wow-delay="0.3s">
				 <h3>Quick Link</h3>
				  <ul class="ftr-nav">
				 	<!-- <li><a href="index.html">Home</a></li>
				 	<li><a href="about.html">About</a></li>
				 	<li><a href="services.html">Services </a></li>
				 	<li><a href="room.html">Rooms</a></li>
				 	<li><a href="contact.html">Contact</a></li> -->
					<li><a href="<?php echo base_url() ?>" >Home</a></li>
					<li><a href="<?php echo base_url() ?>profil" >Profil</a></li>
					<li><a href="<?php echo base_url() ?>layanan" >Layanan</a></li>
					<li><a href="<?php echo base_url() ?>kamar" >Kamar</a></li>
					<li><a href="<?php echo base_url() ?>galeri" >Galeri</a></li>
					<li><a href="<?php echo base_url() ?>kontak">Kontak</a></li>
				</ul>
				 </ul>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<!--footer end here-->
<!--copy rights start here-->
<div class="copy-right">
	<div class="container">
		 <div class="copy-rights-main wow zoomIn" data-wow-delay="0.3s">
    	    <p>© 2017 <?php echo $sitename ?>. All Rights Reserved | Design by  <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
    	 </div>
    </div>
</div>
</body>
</html>
