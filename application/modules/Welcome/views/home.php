<div class="about">
	<div class="container">
		<div class="about-main">
			<div class="about-top">
				<h1>Reservasi Online</h1>
			</div>
			<div class="about-bottom">
				<div class="col-md-5 about-left wow bounceInLeft" data-wow-delay="0.3s">
					<img src="<?php echo TEMPLATE_PATH ?>mr_hotel/images/a1.jpg" alt="" class="img-responsive">
				</div>
				<div class="col-md-7 about-right wow bounceInRight" data-wow-delay="0.3s">
          <div>
            <?php echo $this->session->flashdata("msg") == "" ? "" : $this->session->flashdata("msg") ?>
          </div><br><br>
          <form method="post" action="<?php echo base_url() ?>saveref">
            <div class="form-group">
              <label>Nama</label>
              <input type="text" name="nama" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Alamat</label>
              <input type="text" name="alamat" class="form-control" required>
            </div>
            <div class="row">
              <div class="form-group col-md-6">
                <label>Email</label>
                <input type="email" name="email" class="form-control" required>
              </div>
              <div class="form-group col-md-6">
                <label>Telp.</label>
                <input type="text" name="phone" class="form-control">
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-6">
                <label>Check-In</label>
                <input type="text" name="tglpesan" id="tglpesan" class="form-control" required>
              </div>
              <div class="form-group col-md-6">
                <label>Check-Out</label>
                <input type="text" name="tglcheckout" id="tglcheckout" class="form-control" required>
              </div>
              <div class="form-group col-md-6">
                <label>Jenis Kamar</label>
                <select name="kamar" class="form-control">
                  <option value="">[ Pilih Kamar ]</option>
                  <?php
                  foreach ($kamar->result() as $key) {
                    echo '<option value="'.$key->idkamar.'">'.$key->type.'</option>';
                  }
                  ?>
                </select>
              </div>
              <div class="form-group col-md-6">
                <label>Jumlah Kamar</label>
                <input type="number" name="jmlkamar" class="form-control" required>
              </div>
            </div>
            <div class="form-group">
              <label>Masukan kode dibawah ini</label>
              <?php echo $cap['image'] ?>
              <input type="text" name="cap" class="form-control" required>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary" name="simpan">Simpan</button>
            </div>
          </form>
				</div>
			  <div class="clearfix"> </div>
			</div>
		</div>
	</div>
</div>
<script>
$("#tglpesan, #tglcheckout").datepicker({
  format : 'yyyy/mm/dd'
});
// $("").datepicker();
</script>
