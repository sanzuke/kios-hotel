<!--gallery start here-->
<br>
<div class="gallery" id="gallery">
	<div class="container">
	  <div class="gallery-main wow zoomIn" data-wow-delay="0.3s">
	  	<div class="gallery-top">
	  		<h1>Galeri</h1>
	  	</div>
		<div class="gallery-bott">
			<?php
			foreach ($galleryList->result() as $row) {
				// echo '<option value="'.$row->id.'">'.$row->category_name.'</option>';
			?>
			<div class="col-md-4 col1 gallery-grid">
				<a href="uploads/gallery/<?php echo $row->image ?>" rel="<?php echo $row->judul ?>" class="b-link-stripe b-animate-go  thickbox">
						<figure class="effect-bubba">
							<img class="img-responsive" src="uploads/gallery/<?php echo $row->image ?>" alt="">
							<figcaption>
								<h4 class="gal"><?php echo $row->judul ?></h4>
								<p class="gal1"><?php echo $row->keterangan ?></p>
							</figcaption>
						</figure>
					</a>
			</div>
			<?php } ?>
			     <div class="clearfix"> </div>
			</div>
		</div>
	</div>
</div>
<!--gallery end here-->
<script src="<?php echo TEMPLATE_PATH ?>mr_hotel/js/jquery.chocolat.js"></script>
<link rel="stylesheet" href="<?php echo TEMPLATE_PATH ?>mr_hotel/css/chocolat.css" type="text/css" media="screen" charset="utf-8">
<!--light-box-files -->
<script type="text/javascript" charset="utf-8">
$(function() {
	$('.gallery-grid a').Chocolat();
});
</script>
