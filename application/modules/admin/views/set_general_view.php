<h3><?php echo $judul_halaman ?></h3>
<?php echo $this->Admin_model->showMessage(); ?>
<div class="row">
  <div class="col-md-12">
    <form action="<?php echo base_url() ?>admin/savegeneral" method="post">
      <div class="form-group">
        <label>Nama Web</label>
        <input type="text" name="buname" class="form-control1" value="<?php echo $sitename ?>" />
      </div>
      <div class="form-group">
        <label>Alamat</label>
        <textarea name="bucontact" class="form-control1" rows="5" ><?php echo $alamat ?></textarea>
      </div>
      <div class="form-group">
        <label>Email</label>
        <input type="text" name="email" class="form-control1" value="<?php echo $em ?>" />
      </div>
      <div class="form-group">
        <label>Telp</label>
        <input type="text" name="phone" class="form-control1" value="<?php echo $ph ?>" />
      </div>
      <!-- <div class="form-group">
        <label>Logo</label>
        <input type="file" name="userfile" class="form-control1" >
      </div>
      <div class="form-group col-md-4">
        <img src="<?php echo base_url() ?>" id="img" class="img-responsive">
      </div>
      <div class="clearfix"></div> -->
      <div class="form-group">
        <button name="simpan" class="btn btn-primary">Simpan</button>
      </div>
    </form>
  </div>
</div>
<div class="clearfix"></div>
<script>
	tinymce.init({
	  selector: 'textarea',
	  height: 400,
	  menubar: false,
	  plugins: [
	    'advlist autolink lists link image charmap print preview anchor',
	    'searchreplace visualblocks code fullscreen',
	    'insertdatetime media table contextmenu paste code'
	  ],
	  toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code',
	  content_css: '//www.tinymce.com/css/codepen.min.css'
	});
</script>
