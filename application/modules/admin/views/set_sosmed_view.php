<h3><?php echo $judul_halaman ?></h3>
<?php echo $this->Admin_model->showMessage(); ?>
<div class="row">
  <div class="col-md-12">
    <form method="post" action="<?php echo base_url() ?>admin/savesosmed">
      <div class="form-group">
        <label>Facebook</label>
        <input type="text" name="fb" class="form-control1" value="<?php echo $fb ?>" />
      </div>
      <div class="form-group">
        <label>Twitter</label>
        <input type="text" name="tw" class="form-control1" value="<?php echo $tw ?>" />
      </div>
      <div class="form-group">
        <label>Instagram</label>
        <input type="text" name="ig" class="form-control1" value="<?php echo $ig ?>" />
      </div>
      <div class="form-group">
        <button name="simpan" class="btn btn-primary">Simpan</button>
      </div>
    </form>
  </div>
</div>
<div class="clearfix"></div>
<script>
	tinymce.init({
	  selector: 'textarea',
	  height: 400,
	  menubar: false,
	  plugins: [
	    'advlist autolink lists link image charmap print preview anchor',
	    'searchreplace visualblocks code fullscreen',
	    'insertdatetime media table contextmenu paste code'
	  ],
	  toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code',
	  content_css: '//www.tinymce.com/css/codepen.min.css'
	});
</script>
