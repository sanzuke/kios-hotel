<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends MX_Controller {
	// private $theme = '../../content/theme/kioshotel/index';
	private $theme = '../../content/theme/kioshotel';

	public function index(){
		$this->load->model("Core_model");

		$page_view = $this->theme . '/category'; 

		$data['title_page'] = 'Kategori';
		$this->load->view($page_view, $data);
	}
}

?>